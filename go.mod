module github.com/gherlein/go-pump

go 1.21.4

require (
	github.com/jacobsa/go-serial v0.0.0-20180131005756-15cf729a72d4 // indirect
	golang.org/x/sys v0.16.0 // indirect
)
