package main

import (
	"fmt"
	"io"
	"log"

	"github.com/jacobsa/go-serial/serial"
)

func main() {
	// Set up options.
	options := serial.OpenOptions{
		PortName:              "/dev/ttyUSB0",
		BaudRate:              9600,
		DataBits:              8,
		StopBits:              1,
		MinimumReadSize:       0,
		InterCharacterTimeout: 250,
	}

	// Open the port.
	port, err := serial.Open(options)
	if err != nil {
		log.Fatalf("serial.Open: %v", err)
	}

	// Make sure to close it later.
	defer port.Close()

	/*
			// Write 4 bytes to the port.
			b := []byte{0x41, 0x42, 0x43, 0x44}
			n, err := port.Write(b)
			if err != nil {
				log.Fatalf("port.Write: %v", err)
			}

		fmt.Println("Wrote", n, "bytes.")
	*/

	/*
		packet := make([]byte, 300)
		counter := 0
		var dst byte
		var src byte
		var cmd byte
		var len byte
		//var data [300]byte
	*/
	for {
		buf := make([]byte, 1)
		n, err := port.Read(buf)
		if err != nil {
			if err != io.EOF {
				fmt.Println("Error reading from serial port: ", err)
			}
		} else {
			buf = buf[:n]
			fmt.Printf("[%x] ", buf)
		}
		/*
			if buf[0] == 0x00 {
				packet[counter] = buf[0]
				counter++
			}
			if (packet[0] == 0x00) && buf[0] == 0xff {
				packet[counter] = buf[0]
				counter++
			}
			if (packet[0] == 0x00) && (packet[1] == 0xff) && buf[0] == 0xa5 {
				packet[counter] = buf[0]
				counter++
			}
			if counter == 4 {
				dst = buf[0]
			}
			if counter == 5 {
				src = buf[0]
			}
			if counter == 6 {
				cmd = buf[0]
			}
			if counter == 7 {
				len = buf[0]
			}
			if counter == 7 {
				fmt.Printf("\n dst: %c src: %c cmd: %c len: %c\n", dst, src, cmd, len)
			}
		*/
	}
}
